#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>


int		main(void)
{
	//create socket;
	int		net_socket;
	net_socket = socket(AF_INET, SOCK_STREAM, 0);


	//specify an addr for the socket
	struct	sockaddr_in	server_address;
	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(9002);
	server_address.sin_addr.s_addr = INADDR_ANY;


	//connect
	int 	con_status = connect(net_socket, (struct sockaddr *) &server_address, sizeof(server_address));
	if (con_status == -1)
		printf("Connection with remote socket fails to establish\n");


	//recive
	char	server_response[256];
	recv(net_socket, &server_response, sizeof(server_response), 0);


	//print out our data
	printf("data: %s\n", server_response);

	//close
	close(net_socket);
	return 0;
}
